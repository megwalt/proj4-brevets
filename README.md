# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

Revised by Megan Walter (mwalter2@uoregon.edu)

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The user will input a distance in miles or kilometers, an initial date and time, and using a dropdown option, specify the total brevet distance. As data is added or amended, the page will update using AJAX to calculate the open time and close time of each control point.

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). 

Additional background information is given here (https://rusa.org/pages/rulesForRiders). 

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  

## Additional Details

The algorithms for calculating the windows of time for which each control point is open will round the control point distance to the nearest kilometer (e.g. a control point at 34.4 kms will have the same open and close times as a control point at 34 km). Additionally, timestamps will be rounded to the nearest minute (e.g. if a control point closes at 13:09:31, it will be displayed in the table as closing at 13:10).

If a control point surpasses the specified total brevet distance by more than 20 percent, an error message will be shown to the user so that they can update their input.


