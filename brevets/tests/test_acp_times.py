import acp_times
import arrow

import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)


DATE = arrow.get('2020-12-01', 'YYYY-MM-DD')

def test_french_start():
    """
    test to make sure that the algorithm will correctly determine closing time of 
    control points when they are within 60 kms of the start point (according to the 
    french algorithm).
    """
    assert acp_times.close_time(0, 200, DATE.isoformat()) == DATE.shift(hours=1).isoformat()

def test_over_brevet():
    """
    test that the open and close times of control points that go beyond the specified
    total brevet distance give the same open and close times as if they were equal to the
    total brevet distance.
    """
    assert acp_times.open_time(200, 200, DATE.isoformat()) == acp_times.open_time(205, 200, DATE.isoformat())
    assert acp_times.close_time(200, 200, DATE.isoformat()) == acp_times.close_time(205, 200, DATE.isoformat())

def test_base_case():
    """
    test basic functionality of the calculations.
    """
    assert acp_times.open_time(34, 1000, DATE.isoformat()) == DATE.shift(hours=1).isoformat()
    assert acp_times.close_time(600, 1000, DATE.isoformat()) == DATE.shift(hours=40).isoformat()

def test_rounding_km():
    """
    test that the control point distance is rounded to the nearest km.
    """
    assert acp_times.open_time(34.4, 1000, DATE.isoformat()) == \
        acp_times.open_time(34, 1000, DATE.isoformat())
    assert acp_times.close_time(34.4, 1000, DATE.isoformat()) == \
            acp_times.close_time(34, 1000, DATE.isoformat())

    assert acp_times.open_time(34.7, 1000, DATE.isoformat()) == \
        acp_times.open_time(35, 1000, DATE.isoformat())
    assert acp_times.close_time(34.7, 1000, DATE.isoformat()) == \
            acp_times.close_time(35, 1000, DATE.isoformat())

def test_over_intervals():
    """
    test that if a distance is contained within all intervals, that it will
    return the same result regardless of the total brevet distance.
    """
    assert  acp_times.open_time(34, 1000, DATE.isoformat()) == \
            acp_times.open_time(34, 600, DATE.isoformat()) == \
            acp_times.open_time(34, 400, DATE.isoformat()) == \
            acp_times.open_time(34, 300, DATE.isoformat()) == \
            acp_times.open_time(34, 200, DATE.isoformat())

    assert  acp_times.close_time(34, 1000, DATE.isoformat()) == \
            acp_times.close_time(34, 600, DATE.isoformat()) == \
            acp_times.close_time(34, 400, DATE.isoformat()) == \
            acp_times.close_time(34, 300, DATE.isoformat()) == \
            acp_times.close_time(34, 200, DATE.isoformat())