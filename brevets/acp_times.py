"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#
SPEEDS = {200: (15, 34), 300: (15, 32), 400: (15, 32), 600: (15, 30), 1000: (11.428, 28)}
DEFAULTS = {200: 13.5, 300: 20, 400: 27, 600: 40, 1000: 75}


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, the control distance in kilometers
      brevet_dist_km: number, the nominal distance of the brevet
         in kilometers, which must be one of 200, 300, 400, 600,
         or 1000 (the only official ACP brevet distances)
      brevet_start_time:  An ISO 8601 format date-time string indicating
         the official start time of the brevet
   Returns:
      An ISO 8601 format date string indicating the control open time.
      This will be in the same time zone as the brevet start time.
   """
   intervals = []
   total_time = 0 
   control_dist_km = round(control_dist_km)

   for int_dist in list(SPEEDS.keys()):
      intervals.append(int_dist)
      if control_dist_km <= int_dist or brevet_dist_km == int_dist:
         break
   
   prev_interval = 0
   for interval in intervals[:-1]:
      total_time += (interval - prev_interval) / SPEEDS[interval][1] 
      prev_interval = interval
   
   rest = control_dist_km if control_dist_km < brevet_dist_km else brevet_dist_km

   if not total_time == 0:
      rest -= intervals[-2]
   
   total_time += rest / SPEEDS[intervals[-1]][1]

   hrs = total_time // 1
   mins = int((total_time * 60) % 60)
   secs = int((total_time * 3600) % 60)

   if secs > 30:
      mins += 1
      secs = 0

   retval = arrow.get(brevet_start_time).shift(hours=hrs, minutes=mins, seconds=secs)

   return retval.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, the control distance in kilometers
         brevet_dist_km: number, the nominal distance of the brevet
         in kilometers, which must be one of 200, 300, 400, 600, or 1000
         (the only official ACP brevet distances)
      brevet_start_time:  An ISO 8601 format date-time string indicating
         the official start time of the brevet
   Returns:
      An ISO 8601 format date string indicating the control close time.
      This will be in the same time zone as the brevet start time.
   """
   total_time = 0
   control_dist_km = round(control_dist_km)
   french_start = 60
   french_speed = 20

   if control_dist_km >= brevet_dist_km:
      total_time = DEFAULTS[brevet_dist_km]
   elif control_dist_km <= french_start:
      total_time += control_dist_km / french_speed + 1
   else:
      intervals = []

      for int_dist in list(SPEEDS.keys()):
         intervals.append(int_dist)
         if control_dist_km <= int_dist:
            break
      
      prev_interval = 0
      for interval in intervals[:-1]:
         total_time += (interval - prev_interval) / SPEEDS[interval][0] 
         prev_interval = interval
      
      rest = round(control_dist_km)

      if not total_time == 0:
         rest -= intervals[-2]
      
      total_time += rest / SPEEDS[intervals[-1]][0]

   retval = arrow.get(brevet_start_time).shift(hours=total_time)

   return retval.isoformat()
